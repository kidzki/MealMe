import { Component, OnInit } from '@angular/core';

import { NavController, AlertController, ToastController } from 'ionic-angular';

import { DataService } from './../../providers/data';
import { SearchDetailPage } from './../search-detail/search-detail';


@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
  providers:[DataService]
})
export class SearchPage implements OnInit {
meals = []
  constructor(
    public navCtrl: NavController,
    private dataService: DataService,
    public alertCtrl: AlertController,
    public toastController: ToastController
  ) { }

  doBook(meal){
    let confirm = this.alertCtrl.create({
      title: 'Buchen?',
      message: 'Möchtest du dich für ' + meal.foodname + ' eintragen',
      buttons: [
        {
          text: 'Abbrechen',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Buchen',
          handler: () => {
            let toast = this.toastController.create({
              message: 'Buchung erfolgreich. Guten Appetit',
              duration: 3000,
              position: 'top'
            })
            toast.present();
          }
        }
      ]
    });
    confirm.present();
  }


  onselect(data){
    this.navCtrl.push(SearchDetailPage,data)
  }

  getMeals(){
    this.meals = this.dataService.getMeals()
  }   

  ngOnInit(){
    this.getMeals();
  }


}