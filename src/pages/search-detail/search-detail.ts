import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

/*
  Generated class for the MealDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'search-detail-page',
  templateUrl: 'search-detail.html'
})
export class SearchDetailPage implements OnInit {
  searchDetail

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {}
  
  showAlert(location){
    let alert = this.alertCtrl.create({
      title: 'Navigation startet!',
      subTitle: 'Auf gehts nach ' + location,
      buttons: ['OK']
    });
    alert.present();
  }

  ngOnInit(){
    this.searchDetail = this.navParams.data;
    console.log(this.searchDetail.host.nickname);
  }
}
