import { Component, OnInit } from '@angular/core';

import { NavController, AlertController, FabContainer } from 'ionic-angular';
import { OfferPage } from '../create/create';
import { DatePicker } from 'ionic-native';
import { DataService } from './../../providers/data';
import { SearchDetailPage } from './../search-detail/search-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers:[DataService, SearchDetailPage]
})
export class HomePage implements OnInit {
currentUser;
ListOfMeals;
bookedMeals = [];
offeredMeals = [];

  constructor(
    public navCtrl: NavController,
    private dataService: DataService,
    public searchDetail: SearchDetailPage,
    public alertController: AlertController
  ) { }


    onselect(data){
      this.navCtrl.push(SearchDetailPage,data)
    }

    createOffer(fab: FabContainer){
      fab.close();
      this.navCtrl.push(OfferPage);
    }

    openAlert(fab: FabContainer){
      let alert = this.alertController.create({
        title: 'Coming Soon :)',
        subTitle:'Bald könnt ihr euch zum Ausgehen verabreden.',
        buttons:['OK']
      })
      alert.present();
      fab.close();
    }

    startNavigation(location){
      this.searchDetail.showAlert(location);
    }

    getCurrentUser(){
      this.currentUser = this.dataService.getUserData();
    }

    getMeals(){
      this.ListOfMeals =  this.dataService.getMeals();
    }

    getMealStatus(){
      let meals = this.ListOfMeals;
      for(let i = 0; i <= meals.length-1; i++){
       let array = meals[i].registered_persons
       var match_host = meals[i].host.nickname.indexOf(this.currentUser.nickname)
       if(match_host !== -1){
        this.offeredMeals.push(meals[i]);
       }
       for(let y = 0; y <= array.length-1; y++){
        var match_guest = array[y].nickname.indexOf(this.currentUser.nickname)
        if(match_guest !== -1){
          this.bookedMeals.push(meals[i]);
        }
       }
      }
      console.log(meals)
      console.log(this.bookedMeals)
    }

    ngOnInit(){
      this.getCurrentUser();
      Promise.resolve(this.getMeals()).then(()=>{this.getMealStatus()})
    }
}
