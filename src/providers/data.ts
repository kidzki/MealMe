import { Injectable } from '@angular/core';

import { MEALS } from './mock-meal';
import { USERS } from './mock-user';

@Injectable()
export class DataService {
  constructor() {  }
  getMeals(){ return MEALS; }
  getUserData(){ return USERS[0]; }
}
