import { Meal } from './meal'
import { USERS } from './mock-user'

export const MEALS: Meal[] = [
    {
        id: 1,
        host: USERS[1],
        foodname: 'Spaghetti Bolognese',
        location: 'Büdingen',
        picture:'./../assets/img/spaghetti.JPG',
        time: '1495306800',
        registered_persons:[USERS[2],USERS[5]],
        persons_max: 4,
        price: 4.20,
        tags:'hearty',
        warning:['non_vegetarian','non_gluten']
    },
    {
        id: 2,
        host: USERS[5],
        foodname: 'Gefüllte PEPEroni',
        location: 'Frankfurt am Main',
        picture:'./../assets/img/peperoni.JPG',
        time: '1496257200',
        registered_persons: [USERS[0]],
        persons_max: 4,
        price: 2.00,
        tags:'hearty',
        warning:['non_vegetarian']
    },
    {
        id: 3,
        host: USERS[10],
        foodname: 'Krebbel',
        location: 'Berlin',
        picture:'./../assets/img/berliner.jpg',
        time: '1495998000',
        registered_persons: [USERS[4],USERS[9],USERS[0]],
        persons_max: 3,
        price: 3.00,
        tags:'Bakery',
        warning:['vegetarian']
    },
    {
        id: 4,
        host: USERS[3],
        foodname: 'Tomatensuppe',
        location: 'Hamburg',
        picture:'./../assets/img/tomatensuppe.jpg',
        time: '1495998000',
        registered_persons: [USERS[2],USERS[5],USERS[10],USERS[3],USERS[5],USERS[6]],
        persons_max: 10,
        price: 5.00,
        tags:'Soup',
        warning:['vegetarian']
    },
    {
        id: 5,
        host: USERS[0],
        foodname: 'Hamburger',
        location: 'Darmstadt',
        picture:'./../assets/img/hamburger.jpg',
        time: '1497553200',
        registered_persons: [USERS[2],USERS[5],USERS[10],USERS[3]],
        persons_max: 4,
        price: 6.00,
        tags:'Burger',
        warning:['non_vegetarian','non_gluten']
    }
]