import { User } from './user'

export const USERS: User[] = [
    {
        id: 1,
        nickname: 'kidzki',
        firstname: 'Michael',
        lastname: 'Mustermann',
        picture: './../assets/img/kidzki.JPG',
        location: 'Darmstadt',
        age: "24",
        credit: 13.37
    },
    {
        id: 2,
        nickname: "pepe",
        firstname: "Mylène",
        lastname: "Custance",
        picture: "https://k60.kn3.net/taringa/A/A/0/5/E/8/emmtv1/A06.jpg",
        location: "Mingjiu",
        age: "28/08/1995",
        credit: 0
    }, 
    {
        id: 3,
        nickname: "jnani2",
        firstname: "Léana",
        lastname: "Nani",
        picture: "https://robohash.org/suntvoluptatesut.jpg?size=500x500&set=set1",
        location: "Eckerö",
        age: "23/11/1983",
        credit: 0
    },
    {
        id: 4,
        nickname: "mbratchell3",
        firstname: "Nadège",
        lastname: "Bratchell",
        picture: "https://robohash.org/eaquequisoptio.jpg?size=500x500&set=set1",
        location: "Buzdyak",
        age: "24/12/1970",
        credit: 0
    }, 
    {
        id: 5,
        nickname: "emattiassi4",
        firstname: "Märta",
        lastname: "Mattiassi",
        picture: "https://robohash.org/quiautnemo.jpg?size=500x500&set=set1",
        location: "Ponte",
        age: "22/09/1979",
        credit: 0
    }, 
    {
        id: 6,
        nickname: "tscripps5",
        firstname: "Gösta",
        lastname: "Scripps",
        picture: "https://robohash.org/utquamunde.jpg?size=500x500&set=set1",
        location: "Hengqu",
        age: "04/08/1996",
        credit: 0
    },
    {
        id: 7,
        nickname: "tferrillo6",
        firstname: "Annotés",
        lastname: "Ferrillo",
        picture: "https://robohash.org/voluptatemsuntet.jpg?size=500x500&set=set1",
        location: "Achiaman",
        age: "18/04/1987",
        credit: 0
    },
    {
        id: 8,
        nickname: "epilfold7",
        firstname: "Erwéi",
        lastname: "Pilfold",
        picture: "https://robohash.org/veldoloresqui.jpg?size=500x500&set=set1",
        location: "Mirzec",
        age: "27/10/1972",
        credit: 0
    },
    {
        id: 9,
        nickname: "kfulun8",
        firstname: "Maëline",
        lastname: "Fulun",
        picture: "https://robohash.org/nemoeteveniet.jpg?size=500x500&set=set1",
        location: "El Fureidīs",
        age: "27/04/1975",
        credit: 0
    },
    {
        id: 10,
        nickname: "dnannoni9",
        firstname: "Josée",
        lastname: "Nannoni",
        picture: "https://robohash.org/molestiaevitaererum.jpg?size=500x500&set=set1",
        location: "Sawoi",
        age: "01/03/1985",
        credit: 0
    },
    {
        id: 11,
        nickname: "mwiggin0",
        firstname: "Aí",
        lastname: "Wiggin",
        picture: "https://robohash.org/delenitisitexercitationem.jpg?size=500x500&set=set1",
        location: "Dagup",
        age: "31/05/1992",
        credit: 0
    }
]