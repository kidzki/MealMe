export class User {
    id: number;
    nickname: string;
    firstname: string;
    lastname: string;
    picture: string;
    location: string;
    age: string;
    credit: number;
}