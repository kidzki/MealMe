import { User } from './user'

export class Meal {
    id: number;
    host: User;
    foodname: string;
    location: string;
    picture:string;
    time: string;
    registered_persons: [User];
    persons_max: number;
    price: number;
    tags: string;
    warning:[string];
}